#!/usr/bin/env node

const fs	  = require('fs');
const readline    = require('readline');
const spawn       = require('child_process').spawn;

const SEND_PATH   = 'daemon-send';
const RECV_PATH   = 'daemon-recv';

if (!fs.existsSync(SEND_PATH)){
    spawn('mkfifo', [SEND_PATH]);
}

if (!fs.existsSync(RECV_PATH)){
    spawn('mkfifo', [RECV_PATH]);
}

const DAEMON_SEND = fs.openSync(SEND_PATH, 'w+');
const DAEMON_RECV = fs.openSync(RECV_PATH, 'r+');
const DAEMON_CHLD = spawn('./daemon', [], {
    detached:	true,
    stdio:	[DAEMON_SEND, DAEMON_RECV, 'ignore']
});

const reader      = readline.createInterface(process.stdin, process.stdout);
const writer      = readline.createInterface({input: fs.createReadStream(RECV_PATH)});

reader.setPrompt('>>> ');
reader.prompt();

reader.on('line', function(line) {
    fs.writeSync(DAEMON_SEND, line + '\n');
});

writer.on('line', function(line) {
    console.log("[GOBLIN] " + line);
    reader.prompt();
});
