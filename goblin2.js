#!/usr/bin/env node

const readline    = require('readline');
const spawn       = require('child_process').spawn;
const child       = spawn('./daemon', []);
const reader      = readline.createInterface(process.stdin, process.stdout);
const writer	  = readline.createInterface({input: child.stdout});

reader.setPrompt('>>> ');
reader.prompt();

reader.on('line', function(line) {
    child.stdin.write(line + '\n');
});

writer.on('line', function(line) {
    console.log("[GOBLIN] " + line);
    reader.prompt();
});
