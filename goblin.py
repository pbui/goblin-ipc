#!/usr/bin/env python2.7

import os
import subprocess
import sys

SEND_PATH = 'daemon-send'
RECV_PATH = 'daemon-recv'

try:
    if not os.path.exists(SEND_PATH):
        os.mkfifo(SEND_PATH)
    if not os.path.exists(RECV_PATH):
        os.mkfifo(RECV_PATH)
except OSError as e:
    print e
    sys.exit(1)


DAEMON_SEND = open(SEND_PATH, 'w+')
DAEMON_RECV = open(RECV_PATH, 'r+')

p = subprocess.Popen(['./daemon'], stdin=DAEMON_SEND, stdout=DAEMON_RECV)

try:
    while True:
        line = raw_input('>>> ')
        DAEMON_SEND.write(line + '\n')
        DAEMON_SEND.flush()

        print '[GOBLIN]', DAEMON_RECV.readline()
except (KeyboardInterrupt, EOFError):
    os.unlink(SEND_PATH)
    os.unlink(RECV_PATH)
