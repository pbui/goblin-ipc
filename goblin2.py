#!/usr/bin/env python2.7

import os
import subprocess
import sys

p = subprocess.Popen(['./daemon'], stdin=subprocess.PIPE, stdout=subprocess.PIPE)

try:
    while True:
        line = raw_input('>>> ')
        p.stdin.write(line + '\n')
        p.stdin.flush()

        print '[GOBLIN]', p.stdout.readline()
except KeyboardInterrupt:
    sys.exit(0)
